#pragma once
#ifndef Weapon_H
#define Weapon_H
#include <string>
using namespace std;
class Weapon
{
public:
	Weapon(string WeaponName, int WeaponDamage);
	string getWeaponName();
	int getWeaponDamage();
private:
	string WeaponName;
	int WeaponDamage;
};
#endif // !Weapon_H