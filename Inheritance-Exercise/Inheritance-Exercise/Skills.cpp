#include "Skills.h"
#include "Character.h"
#include "Weapon.h"
#include <iostream>
#include <string>
using namespace std;


Skills::Skills(string name)
{
	this->skillName = name;
}
int Skills::use(Character* Caster)
{
	return Caster->getWeaponDamage();
}
BasicAttack::BasicAttack(string skillName)
{
	this->skillName = skillName;
}

int BasicAttack::use(Character* Caster)
{
	cout << "Using Basic Attack" << endl;
	int damage = Caster->getWeaponDamage();
	return damage;
}

int Critical::use(Character* Caster)
{
	cout << "Using Critical" << endl;
	int critChance = rand() % 100 + 1,
		damage;
	if (critChance <= 20)
	{
		cout << "Critical Success!!" << endl;
		damage = Caster->getWeaponDamage() * 2;

	}
	else
	{
		damage = Caster->getWeaponDamage();
	}
	return damage;
}

int Syphon::use(Character* Caster)
{
	int damage;
	cout << "Using Syphon" << endl;
	damage = Caster->getWeaponDamage();
	Caster->setHP(Caster->getHP() + Caster->getWeaponDamage()*1/4);
	return damage;
}

int Vengeance::use(Character *Caster)
{
	cout << "Using Vengeance"  << endl;
	Caster->setHP(Caster->getHP() - (Caster->getHP*.25));
	if (Caster->getHP() <= 0)
	{
		Caster->setHP(1);
	}
	int damage = Caster->getWeaponDamage() * 2 + Caster->getWeaponDamage();
	return damage;
}

int DopelBlade::use(Character * Caster,Character* Target)
{
	cout << "Using Dopel Blade" << endl;
	int damage;
	Character * DoppelCopy = Caster;
	damage = DoppelCopy->attack(DoppelCopy,Target);
	return damage;
}



string Skills::getSkillName()
{
	return skillName;
}
