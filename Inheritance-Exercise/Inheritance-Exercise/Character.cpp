#include "Character.h"
#include "Weapon.h"
#include "Skills.h"
#include <String>
#include <vector>
Character::Character(int HP, int  MP, vector<Skills> SkillList, Weapon MainWeapon)
{
	playerHP = HP;
	playerMP = MP;
	playerSkills = SkillList;
	playerWeapon = MainWeapon;
}

int Character::attack(Character* Caster,Character * Target)
{
	int randomSkill = rand() % 5;
	int damage;
	Caster->castSkill(Caster, randomSkill);
	return damage;
}

void Character::setHP(int HP)
{
	this->playerHP = HP;
}

void Character::setMP(int MP)
{
	this->playerMP = MP;
}

int Character::getHP()
{
	return playerHP;
}

int Character::getMP()
{
	return playerMP;
}


string Character::getWeaponName()
{
	return playerWeapon.getWeaponName();
}

int Character::getWeaponDamage()
{
	return playerWeapon.getWeaponDamage();
}

void Character::castSkill(Character * Caster, int skill)
{
}

void Character::castSkill(Character* Caster,int skill)
{
	playerSkills[skill].use(Caster);
}
