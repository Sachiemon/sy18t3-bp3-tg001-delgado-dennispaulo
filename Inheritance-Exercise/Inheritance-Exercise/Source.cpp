#include "Character.h"
#include "Skills.h"
#include "Weapon.h"
#include <iostream>
#include <string>
#include <vector>

int main()
{
	BasicAttack *s1 = new BasicAttack("Basic Attack");
	Critical *s2 = new Critical("Critical");
	Syphon *s3 = new Syphon("Syphon");
	Vengeance *s4 = new Vengeance("Syphon");
	DopelBlade *s5 = new DopelBlade("Dopel Blade");
	vector<Skills> SkillList;
	SkillList.push_back(*s1);
	SkillList.push_back(*s2);
	SkillList.push_back(*s3);
	SkillList.push_back(*s4);
	SkillList.push_back(*s5);
	Weapon *sword = new Weapon("Sword", 100);
	Character *player1=new Character(100, 100, SkillList,sword);
	Character *player2=new Character(100, 100, SkillList, sword);
	player1->attack(player1, player2);
	system("pause");
	return 0;
}