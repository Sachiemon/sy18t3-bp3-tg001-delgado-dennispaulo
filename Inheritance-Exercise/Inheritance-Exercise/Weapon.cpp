#include "Weapon.h"
using namespace std;
Weapon::Weapon(string WeaponName, int WeaponDamage)
{
	this->WeaponName = WeaponName;
	this->WeaponDamage = WeaponDamage;
}
string Weapon::getWeaponName()
{
	return WeaponName;
}

int Weapon::getWeaponDamage()
{
	return WeaponDamage;
}