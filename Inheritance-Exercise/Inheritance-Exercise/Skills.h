#pragma once
#ifndef Skills_H
#define Skills_H
#include <string>

class Skills
{
public:
	Skills(string name);
	string getSkillName();
	virtual int use(Character *Caster);
protected:
	string skillName;
};
class BasicAttack : public Skills
{
public:
	BasicAttack(string skillName) : Skills(skillName);
	int use(Character* Caster);
};
class Critical : public Skills
{
public:
	Critical(string skillName) : Skills(skillName)
	{

	};
	int use(Character* Caster);
};
class Syphon : public Skills
{
public:
	Syphon(string skillName) : Skills(skillName)
	{

	};
	int use(Character* Caster);
};
class Vengeance : public Skills
{
public:
	Vengeance(string skillName) : Skills(skillName)
	{

	};
	int use(Character* Caster);
};
class DopelBlade : public Skills
{
public:
	DopelBlade(string skillName) : Skills(skillName)
	{

	};
	int use(Character* Caster, Character* Target);
};
#endif // !Character_H