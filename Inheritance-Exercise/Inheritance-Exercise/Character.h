#pragma once
#ifndef Character_H
#define Character_H
#include "Skills.h"
#include "Weapon.h"
#include <vector>
using namespace std;
class Character
{
public:
	Character(int HP, int MP, vector<Skills> Skills, Weapon *MainWeapon);
	int attack(Character* Caster,Character* Target);
	void setHP(int HP);
	void setMP(int MP);
	int getHP();
	int getMP();
	string getWeaponName();
	int getWeaponDamage();
	void castSkill(Character * Caster,int skill);
private:
	int playerHP;
	int playerMP;
	vector<Skills> playerSkills;
	Weapon playerWeapon;

};
#endif // !Character_H;